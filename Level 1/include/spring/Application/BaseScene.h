#pragma once

#include <qpushbutton.h>
#include <qgridlayout.h>
#include "qcustomplot.h"
#include <spring\Framework\IScene.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
			private slots:
				void returnClick();

		private:
			QWidget *centralWidget;
			QCustomPlot *customPlot;
			QGridLayout *gridLayout;
			QPushButton *stopButton;
			QPushButton *startButton;
			QPushButton *returnButton;
			QSpacerItem *horizontalSpacer;
			QGridLayout *centralWidgetLayout;

		public:
			explicit BaseScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
