#include <spring\Application\BaseScene.h>

namespace Spring
{
	BaseScene::BaseScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void BaseScene::createScene()
	{
		std::string windowTitle = boost::any_cast<std::string>(m_TransientDataCollection.find("appNameLEdit")->second);
		m_uMainWindow->setWindowTitle(QString::fromStdString(windowTitle));
		createGUI();
		connect(returnButton, SIGNAL(clicked()), this, SLOT(returnClick()));
	}

	void BaseScene::release()
	{

	}

	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		centralWidgetLayout = new QGridLayout(centralWidget);
		centralWidgetLayout->setSpacing(5);
		centralWidgetLayout->setContentsMargins(10, 10, 10, 10);
		centralWidgetLayout->setObjectName(QStringLiteral("centralWidgetLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(5);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));

		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("listView"));
		gridLayout->addWidget(customPlot, 0, 0, 1, 4);

		startButton = new QPushButton(centralWidget);
		startButton->setObjectName(QStringLiteral("startButton"));
		gridLayout->addWidget(startButton, 2, 1, 1, 1);

		stopButton = new QPushButton(centralWidget);
		stopButton->setObjectName(QStringLiteral("stopButton"));
		gridLayout->addWidget(stopButton, 2, 2, 1, 1);

		returnButton = new QPushButton(centralWidget);
		returnButton->setObjectName(QStringLiteral("returnButton"));
		gridLayout->addWidget(returnButton, 2, 3, 1, 1);

		startButton->setText("Start");
		stopButton->setText("Stop");
		returnButton->setText("Back");

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

		centralWidgetLayout->addLayout(gridLayout, 0, 0, 1, 1);
		m_uMainWindow.get()->setCentralWidget(centralWidget);
	}

	void BaseScene::returnClick()
	{
		emit SceneChange("InitialScene");
	}
}
