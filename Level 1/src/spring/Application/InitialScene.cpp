#include <spring\Application\InitialScene.h>

namespace Spring
{
	InitialScene::InitialScene(const std::string &ac_szSceneName) : IScene( ac_szSceneName)
	{

	}

	void InitialScene::createScene()
	{
		createGUI();
		connect(okButton, SIGNAL(clicked()), this, SLOT(mf_OkButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	void InitialScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		centralWidgetLayout = new QGridLayout(centralWidget);
		centralWidgetLayout->setSpacing(5);
		centralWidgetLayout->setContentsMargins(10, 10, 10, 10);
		centralWidgetLayout->setObjectName(QStringLiteral("centralWidgetLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(5);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));

		appNameLabel = new QLabel(centralWidget);
		appNameLabel->setObjectName(QStringLiteral("appNameLabel"));
		gridLayout->addWidget(appNameLabel, 1, 1, 1, 1);

		sampleRateLabel = new QLabel(centralWidget);
		sampleRateLabel->setObjectName(QStringLiteral("sampleRateLabel"));
		gridLayout->addWidget(sampleRateLabel, 2, 1, 1, 1);

		displayTimeLabel = new QLabel(centralWidget);
		displayTimeLabel->setObjectName(QStringLiteral("displayTimeLabel"));
		gridLayout->addWidget(displayTimeLabel, 3, 1, 1, 1);

		refreshRateLabel = new QLabel(centralWidget);
		refreshRateLabel->setObjectName(QStringLiteral("refreshRateLabel"));
		gridLayout->addWidget(refreshRateLabel, 4, 1, 1, 1);

		okButton = new QPushButton(centralWidget);
		okButton->setObjectName(QStringLiteral("okButton"));
		gridLayout->addWidget(okButton, 5, 3, 1, 1);

		appNameLEdit = new QLineEdit(centralWidget);
		std::string defaultAppName = boost::any_cast<std::string>(m_TransientDataCollection.find("appNameLEdit")->second);
		appNameLEdit->setObjectName(QStringLiteral("appNameLEdit"));
		appNameLEdit->setText(QString::fromStdString(defaultAppName));
		gridLayout->addWidget(appNameLEdit, 1, 2, 1, 1);

		sampleRateDSBox = new QDoubleSpinBox(centralWidget);
		unsigned int defaultSampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("sampleRateDSBox")->second);
		sampleRateDSBox->setObjectName(QStringLiteral("sampleRateDSBox"));
		sampleRateDSBox->setDecimals(0);
		sampleRateDSBox->setMaximum(100000);
		sampleRateDSBox->setValue(defaultSampleRate);
		gridLayout->addWidget(sampleRateDSBox, 2, 2, 1, 1);

		displayTimeDSBox = new QDoubleSpinBox(centralWidget);
		double defaultDisplayTime = boost::any_cast<double>(m_TransientDataCollection.find("displayTimeDSBox")->second);
		displayTimeDSBox->setObjectName(QStringLiteral("displayTimeDSBox"));
		displayTimeDSBox->setValue(defaultDisplayTime);
		gridLayout->addWidget(displayTimeDSBox, 3, 2, 1, 1);

		refreshRateDSBox = new QDoubleSpinBox(centralWidget);
		unsigned int defaultRefreshRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("refreshRateDSBox")->second);
		refreshRateDSBox->setObjectName(QStringLiteral("refreshRateDSBox"));
		refreshRateDSBox->setDecimals(0);
		refreshRateDSBox->setValue(defaultRefreshRate);
		gridLayout->addWidget(refreshRateDSBox, 4, 2, 1, 1);

		appNameLabel->setText("Application Name:");
		refreshRateLabel->setText("Refresh Rate:");
		displayTimeLabel->setText("Display Time:");
		sampleRateLabel->setText("Sample Rate:");
		okButton->setText("OK");

		horizontalSpacer1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(horizontalSpacer1, 3, 0, 1, 1);

		horizontalSpacer2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(horizontalSpacer2, 3, 3, 1, 1);

		verticalSpacer1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
		gridLayout->addItem(verticalSpacer1, 0, 2, 1, 1);

		verticalSpacer2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
		gridLayout->addItem(verticalSpacer2, 5, 2, 1, 1);

		centralWidgetLayout->addLayout(gridLayout, 0, 0, 1, 1);
		m_uMainWindow.get()->setCentralWidget(centralWidget);
	}

	void InitialScene::mf_OkButton()
	{
		std::string currentAppName = appNameLEdit->text().toStdString();
		m_TransientDataCollection.erase("appNameLEdit");
		m_TransientDataCollection.emplace("appNameLEdit", currentAppName);

		const unsigned int currentSampleRate = sampleRateDSBox->value();
		m_TransientDataCollection.erase("sampleRateDSBox");
		m_TransientDataCollection.emplace("sampleRateDSBox", currentSampleRate);

		const double currentDisplayTime = displayTimeDSBox->value();
		m_TransientDataCollection.erase("displayTimeDSBox");
		m_TransientDataCollection.emplace("displayTimeDSBox", currentDisplayTime);

		const unsigned int currentRefreshRate = refreshRateDSBox->value();
		m_TransientDataCollection.erase("refreshRateDSBox");
		m_TransientDataCollection.emplace("refreshRateDSBox", currentRefreshRate);

		emit SceneChange("BaseScene");
	}
}
