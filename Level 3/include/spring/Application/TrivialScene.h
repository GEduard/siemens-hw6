#pragma once

#include <iomanip>
#include <sstream>
#include <Windows.h>
#include <qmessagebox.h>
#include <spring\Framework\IScene.h>

typedef double(__cdecl *TrivialMethod)(const int&, const double&);

namespace Spring
{
	class TrivialScene : public IScene
	{
		private:
			QMessageBox messageBox;

		public:
			explicit TrivialScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
