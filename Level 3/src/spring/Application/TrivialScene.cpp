#include <spring\Application\TrivialScene.h>

namespace Spring
{
	TrivialScene::TrivialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void TrivialScene::createScene()
	{
		std::string windowTitle = boost::any_cast<std::string>(m_TransientDataCollection.find("appNameLEdit")->second);
		m_uMainWindow->setWindowTitle(QString::fromStdString(windowTitle));
		createGUI();
	}

	void TrivialScene::release()
	{

	}

	void TrivialScene::createGUI()
	{
		//We grab the required data from the map so we can later pass it as parameters for our method
		unsigned int sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("sampleRateDSBox")->second);
		double displayTime = boost::any_cast<double>(m_TransientDataCollection.find("displayTimeDSBox")->second);

		//We load the DLL in order to use the method for the trivial calculation
		HINSTANCE hModule = LoadLibrary(TEXT("TrivialLibrary.dll"));
		TrivialMethod tMethod = (TrivialMethod)GetProcAddress(hModule, "samplesNumber");

		//We ready the message to be displayed in the message box
		std::string messageText = "The number of samples is: ";

		if (tMethod)
		{
			//We pass the previous values as parameters for the method to get the required result which we store in a variable
			const double result = tMethod(sampleRate, displayTime);

			//We round the result(double type) to only two decimals for ease of display and build the final string for the message
			std::stringstream samplesNumber;
			samplesNumber << std::fixed << std::setprecision(2) << result;
			messageText.append(samplesNumber.str());
		}

		FreeLibrary(hModule);

		//We display the information for the user
		messageBox.setText(QString::fromStdString(messageText));
		messageBox.exec();
	}
}
