#pragma once

extern "C"
{
	__declspec(dllexport) double __cdecl samplesNumber(const int &sampleRate, const double &displayTime);
}