#include <spring\Application\BaseScene.h>
#include <spring\Application\InitialScene.h>
#include <spring\Application\ApplicationModel.h>

const std::string& initialSceneName = "InitialScene";
const std::string& baseSceneName = "BaseScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		IScene *baseScene = new BaseScene(baseSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);
		m_Scenes.emplace(baseSceneName, baseScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//We check wether the file exists or not(when the file exists, it can be found in the Loader's project folder)
		//If it does than we extract the data and load it into our map, else we load the default values
		if (FILE *transientData = fopen("TransientData.txt", "r"))
		{
			//The way we store data in our file is: keyName, keyValue
			std::string keyName, keyValue;
			std::ifstream fileData("TransientData.txt");
			while (fileData >> keyName)
			{
				fileData >> keyValue;
				m_TransientData.emplace(keyName, keyValue);
			}
			fileData.close();
			fclose(transientData);
		}
		else
		{
			/*The reason why I chose to store everything as a string it's because if the file doesn't exist
			if we were to use numerical type values, or any other type, it would cause the program to crash*/
			//The error would be caused by any_cast trying to convert to a different type value, than the one stored in the map, throwing an exception
			std::string appName = "DefaultName";
			m_TransientData.emplace("appNameLEdit", appName);
			std::string sampleRate = "25600";
			m_TransientData.emplace("sampleRateDSBox", sampleRate);
			std::string displayTime = "0.10";
			m_TransientData.emplace("displayTimeDSBox", displayTime);
			std::string refreshRate = "1";
			m_TransientData.emplace("refreshRateDSBox", refreshRate);
		}
	}
}
