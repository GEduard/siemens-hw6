#include <spring\Application\BaseScene.h>

namespace Spring
{
	BaseScene::BaseScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void BaseScene::createScene()
	{
		std::string windowTitle = boost::any_cast<std::string>(m_TransientDataCollection.find("appNameLEdit")->second);
		m_uMainWindow->setWindowTitle(QString::fromStdString(windowTitle));
	}

	void BaseScene::release()
	{

	}
}
