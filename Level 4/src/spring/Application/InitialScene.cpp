#include <spring\Application\InitialScene.h>

namespace Spring
{
	InitialScene::InitialScene(const std::string &ac_szSceneName) : IScene( ac_szSceneName)
	{

	}

	void InitialScene::createScene()
	{
		createGUI();
		connect(okButton, SIGNAL(clicked()), this, SLOT(mf_OkButton()));
	}

	void InitialScene::release()
	{

		//Since this method is also called when we close the application, I figured it's best to save all the data here
		std::string keyValue;
		std::ofstream fileData("TransientData.txt");

		//This is part of the code that can be found in the OK's button handler
		//I figured it is better to grab all the data at once, even if there are specific methods to check when a QT field's value changed
		std::string currentAppName = appNameLEdit->text().toStdString();
		m_TransientDataCollection.erase("appNameLEdit");
		m_TransientDataCollection.emplace("appNameLEdit", currentAppName);

		const unsigned int currentSampleRate = sampleRateDSBox->value();
		m_TransientDataCollection.erase("sampleRateDSBox");
		m_TransientDataCollection.emplace("sampleRateDSBox", std::to_string(currentSampleRate));

		const double currentDisplayTime = displayTimeDSBox->value();
		m_TransientDataCollection.erase("displayTimeDSBox");
		m_TransientDataCollection.emplace("displayTimeDSBox", std::to_string(currentDisplayTime));

		const unsigned int currentRefreshRate = refreshRateDSBox->value();
		m_TransientDataCollection.erase("refreshRateDSBox");
		m_TransientDataCollection.emplace("refreshRateDSBox", std::to_string(currentRefreshRate));

		for (const auto& key : m_TransientDataCollection)
		{
			keyValue = boost::any_cast<std::string>(key.second);
			fileData << key.first << " ";
			fileData << keyValue << std::endl;
		}

		fileData.close();

		delete centralWidget;
	}

	void InitialScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		centralWidgetLayout = new QGridLayout(centralWidget);
		centralWidgetLayout->setSpacing(5);
		centralWidgetLayout->setContentsMargins(10, 10, 10, 10);
		centralWidgetLayout->setObjectName(QStringLiteral("centralWidgetLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(5);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));

		appNameLabel = new QLabel(centralWidget);
		appNameLabel->setObjectName(QStringLiteral("appNameLabel"));
		gridLayout->addWidget(appNameLabel, 1, 1, 1, 1);

		sampleRateLabel = new QLabel(centralWidget);
		sampleRateLabel->setObjectName(QStringLiteral("sampleRateLabel"));
		gridLayout->addWidget(sampleRateLabel, 2, 1, 1, 1);

		displayTimeLabel = new QLabel(centralWidget);
		displayTimeLabel->setObjectName(QStringLiteral("displayTimeLabel"));
		gridLayout->addWidget(displayTimeLabel, 3, 1, 1, 1);

		refreshRateLabel = new QLabel(centralWidget);
		refreshRateLabel->setObjectName(QStringLiteral("refreshRateLabel"));
		gridLayout->addWidget(refreshRateLabel, 4, 1, 1, 1);

		okButton = new QPushButton(centralWidget);
		okButton->setObjectName(QStringLiteral("okButton"));
		gridLayout->addWidget(okButton, 5, 3, 1, 1);

		//We use lexical_cast togheter with any_cast(lexical_cast is specially designed for converting a string to a numerical type)
		//We need any_cast because first convert the keyValue to a string because lexical_cast takes as argument a string, not a boost::any value
		//If we were to use lexical_cast only we would get an argument exception error, if we were to use any_cast only it would throw a bad_any_cast exception

		//This is where we store the keyValue as a string, before we use lexical_cast to convert it to our required type
		std::string fieldValue;

		appNameLEdit = new QLineEdit(centralWidget);
		std::string defaultAppName = boost::any_cast<std::string>(m_TransientDataCollection.find("appNameLEdit")->second);
		appNameLEdit->setObjectName(QStringLiteral("appNameLEdit"));
		appNameLEdit->setText(QString::fromStdString(defaultAppName));
		gridLayout->addWidget(appNameLEdit, 1, 2, 1, 1);

		sampleRateDSBox = new QDoubleSpinBox(centralWidget);
		fieldValue = boost::any_cast<std::string>(m_TransientDataCollection.find("sampleRateDSBox")->second);
		unsigned int defaultSampleRate = boost::lexical_cast<unsigned>(fieldValue);
		sampleRateDSBox->setObjectName(QStringLiteral("sampleRateDSBox"));
		sampleRateDSBox->setDecimals(0);
		sampleRateDSBox->setMaximum(100000);
		sampleRateDSBox->setValue(defaultSampleRate);
		gridLayout->addWidget(sampleRateDSBox, 2, 2, 1, 1);

		displayTimeDSBox = new QDoubleSpinBox(centralWidget);
		fieldValue = boost::any_cast<std::string>(m_TransientDataCollection.find("displayTimeDSBox")->second);
		double defaultDisplayTime = boost::lexical_cast<double>(fieldValue);
		displayTimeDSBox->setObjectName(QStringLiteral("displayTimeDSBox"));
		displayTimeDSBox->setValue(defaultDisplayTime);
		gridLayout->addWidget(displayTimeDSBox, 3, 2, 1, 1);

		refreshRateDSBox = new QDoubleSpinBox(centralWidget);
		fieldValue = boost::any_cast<std::string>(m_TransientDataCollection.find("refreshRateDSBox")->second);
		unsigned int defaultRefreshRate = boost::lexical_cast<unsigned>(fieldValue);
		refreshRateDSBox->setObjectName(QStringLiteral("refreshRateDSBox"));
		refreshRateDSBox->setDecimals(0);
		refreshRateDSBox->setValue(defaultRefreshRate);
		gridLayout->addWidget(refreshRateDSBox, 4, 2, 1, 1);

		appNameLabel->setText("Application Name:");
		refreshRateLabel->setText("Refresh Rate:");
		displayTimeLabel->setText("Display Time:");
		sampleRateLabel->setText("Sample Rate:");
		okButton->setText("OK");

		horizontalSpacer1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(horizontalSpacer1, 3, 0, 1, 1);

		horizontalSpacer2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(horizontalSpacer2, 3, 3, 1, 1);

		verticalSpacer1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
		gridLayout->addItem(verticalSpacer1, 0, 2, 1, 1);

		verticalSpacer2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
		gridLayout->addItem(verticalSpacer2, 5, 2, 1, 1);

		centralWidgetLayout->addLayout(gridLayout, 0, 0, 1, 1);
		m_uMainWindow.get()->setCentralWidget(centralWidget);
	}

	void InitialScene::mf_OkButton()
	{
		//I convert all the data to string, for the reasons listed above
		std::string currentAppName = appNameLEdit->text().toStdString();
		m_TransientDataCollection.erase("appNameLEdit");
		m_TransientDataCollection.emplace("appNameLEdit", currentAppName);

		const unsigned int currentSampleRate = sampleRateDSBox->value();
		m_TransientDataCollection.erase("sampleRateDSBox");
		m_TransientDataCollection.emplace("sampleRateDSBox", std::to_string(currentSampleRate));

		const double currentDisplayTime = displayTimeDSBox->value();
		m_TransientDataCollection.erase("displayTimeDSBox");
		m_TransientDataCollection.emplace("displayTimeDSBox", std::to_string(currentDisplayTime));

		const unsigned int currentRefreshRate = refreshRateDSBox->value();
		m_TransientDataCollection.erase("refreshRateDSBox");
		m_TransientDataCollection.emplace("refreshRateDSBox", std::to_string(currentRefreshRate));

		emit SceneChange("BaseScene");
	}
}
